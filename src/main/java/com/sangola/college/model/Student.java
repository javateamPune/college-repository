package com.sangola.college.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Student {
	@Id
	Long studentaadhaar;

	Integer studentRoll;

	@Column(length = 30)
	String studentFirstName;

	@Column(length = 30)
	String studentMiddleName;

	@Column(length = 30)
	String studentlastName;

	LocalDate studentDob;

	@Column(length = 10)
	String studentGender;
	@Column(length = 30)
	String studentMaritalStatus;

	@Column(length = 150)
	String studentAddress;

	@Column(length = 60)
	String studentEmail;

	@Column(length = 20)
	String studentPassword;

	Long studentMobile;

	@Column(length = 13)
	Long parentMobile;

	@ManyToOne
	ClassAndFees classAndFee;

	@Column(length = 10)
	String studentDiv;

	public ClassAndFees getClassAndFee() {
		return classAndFee;
	}

	public void setClassAndFee(ClassAndFees classAndFee) {
		this.classAndFee = classAndFee;
	}

	public String getStudentDiv() {
		return studentDiv;
	}

	public void setStudentDiv(String studentDiv) {
		this.studentDiv = studentDiv;
	}

	public Long getStudentaadhaar() {
		return studentaadhaar;
	}

	public void setStudentaadhaar(Long studentaadhaar) {
		this.studentaadhaar = studentaadhaar;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getStudentFirstName() {
		return studentFirstName;
	}

	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}

	public String getStudentMiddleName() {
		return studentMiddleName;
	}

	public void setStudentMiddleName(String studentMiddleName) {
		this.studentMiddleName = studentMiddleName;
	}

	public String getStudentlastName() {
		return studentlastName;
	}

	public void setStudentlastName(String studentlastName) {
		this.studentlastName = studentlastName;
	}

	public LocalDate getStudentDob() {
		return studentDob;
	}

	public void setStudentDob(LocalDate studentDob) {
		this.studentDob = studentDob;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public String getStudentMaritalStatus() {
		return studentMaritalStatus;
	}

	public void setStudentMaritalStatus(String studentMaritalStatus) {
		this.studentMaritalStatus = studentMaritalStatus;
	}

	public String getStudentAddress() {
		return studentAddress;
	}

	public void setStudentAddress(String studentAddress) {
		this.studentAddress = studentAddress;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	public String getStudentPassword() {
		return studentPassword;
	}

	public void setStudentPassword(String studentPassword) {
		this.studentPassword = studentPassword;
	}

	public Long getStudentMobile() {
		return studentMobile;
	}

	public void setStudentMobile(Long studentMobile) {
		this.studentMobile = studentMobile;
	}

	public Long getParentMobile() {
		return parentMobile;
	}

	public void setParentMobile(Long parentMobile) {
		this.parentMobile = parentMobile;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classAndFee == null) ? 0 : classAndFee.hashCode());
		result = prime * result + ((parentMobile == null) ? 0 : parentMobile.hashCode());
		result = prime * result + ((studentAddress == null) ? 0 : studentAddress.hashCode());
		result = prime * result + ((studentDiv == null) ? 0 : studentDiv.hashCode());
		result = prime * result + ((studentDob == null) ? 0 : studentDob.hashCode());
		result = prime * result + ((studentEmail == null) ? 0 : studentEmail.hashCode());
		result = prime * result + ((studentFirstName == null) ? 0 : studentFirstName.hashCode());
		result = prime * result + ((studentGender == null) ? 0 : studentGender.hashCode());
		result = prime * result + ((studentMaritalStatus == null) ? 0 : studentMaritalStatus.hashCode());
		result = prime * result + ((studentMiddleName == null) ? 0 : studentMiddleName.hashCode());
		result = prime * result + ((studentMobile == null) ? 0 : studentMobile.hashCode());
		result = prime * result + ((studentPassword == null) ? 0 : studentPassword.hashCode());
		result = prime * result + ((studentRoll == null) ? 0 : studentRoll.hashCode());
		result = prime * result + ((studentaadhaar == null) ? 0 : studentaadhaar.hashCode());
		result = prime * result + ((studentlastName == null) ? 0 : studentlastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (classAndFee == null) {
			if (other.classAndFee != null)
				return false;
		} else if (!classAndFee.equals(other.classAndFee))
			return false;
		if (parentMobile == null) {
			if (other.parentMobile != null)
				return false;
		} else if (!parentMobile.equals(other.parentMobile))
			return false;
		if (studentAddress == null) {
			if (other.studentAddress != null)
				return false;
		} else if (!studentAddress.equals(other.studentAddress))
			return false;
		if (studentDiv == null) {
			if (other.studentDiv != null)
				return false;
		} else if (!studentDiv.equals(other.studentDiv))
			return false;
		if (studentDob == null) {
			if (other.studentDob != null)
				return false;
		} else if (!studentDob.equals(other.studentDob))
			return false;
		if (studentEmail == null) {
			if (other.studentEmail != null)
				return false;
		} else if (!studentEmail.equals(other.studentEmail))
			return false;
		if (studentFirstName == null) {
			if (other.studentFirstName != null)
				return false;
		} else if (!studentFirstName.equals(other.studentFirstName))
			return false;
		if (studentGender == null) {
			if (other.studentGender != null)
				return false;
		} else if (!studentGender.equals(other.studentGender))
			return false;
		if (studentMaritalStatus == null) {
			if (other.studentMaritalStatus != null)
				return false;
		} else if (!studentMaritalStatus.equals(other.studentMaritalStatus))
			return false;
		if (studentMiddleName == null) {
			if (other.studentMiddleName != null)
				return false;
		} else if (!studentMiddleName.equals(other.studentMiddleName))
			return false;
		if (studentMobile == null) {
			if (other.studentMobile != null)
				return false;
		} else if (!studentMobile.equals(other.studentMobile))
			return false;
		if (studentPassword == null) {
			if (other.studentPassword != null)
				return false;
		} else if (!studentPassword.equals(other.studentPassword))
			return false;
		if (studentRoll == null) {
			if (other.studentRoll != null)
				return false;
		} else if (!studentRoll.equals(other.studentRoll))
			return false;
		if (studentaadhaar == null) {
			if (other.studentaadhaar != null)
				return false;
		} else if (!studentaadhaar.equals(other.studentaadhaar))
			return false;
		if (studentlastName == null) {
			if (other.studentlastName != null)
				return false;
		} else if (!studentlastName.equals(other.studentlastName))
			return false;
		return true;
	}

	
	
	
	@Override
	public String toString() {
		return "Student [studentaadhaar=" + studentaadhaar + ", studentRoll=" + studentRoll + ", studentFirstName="
				+ studentFirstName + ", studentMiddleName=" + studentMiddleName + ", studentlastName=" + studentlastName
				+ ", studentDob=" + studentDob + ", studentGender=" + studentGender + ", studentMaritalStatus="
				+ studentMaritalStatus + ", studentAddress=" + studentAddress + ", studentEmail=" + studentEmail
				+ ", studentPassword=" + studentPassword + ", studentMobile=" + studentMobile + ", parentMobile="
				+ parentMobile + ", studentClass=" + classAndFee + ", studentDiv=" + studentDiv + "]";
	}

}
