package com.sangola.college.model;

import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Short  eventId;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@Basic
	@NotNull
	LocalDateTime startDate;
	
	@Basic
	LocalDateTime endDate;

	final LocalDateTime creationDate = LocalDateTime.now();
	
	@NotNull
	String Name;
	
	@NotNull
	String Description;
	
	
	String organiser;
	
	@NotNull
	String venue;

	public  Short getEventId() {
		return eventId;
	}

	public void setEventId(Short eventId) {
		this.eventId = eventId;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getOrganiser() {
		return organiser;
	}

	public void setOrganiser(String organiser) {
		this.organiser = organiser;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	@Override
	public String toString() {
		return "Event [eventId=" + eventId + ", startDate=" + startDate + ", endDate=" + endDate + ", creationDate="
				+ creationDate + ", Name=" + Name + ", Description=" + Description + ", organiser=" + organiser
				+ ", venue=" + venue + "]";
	}	
}
