package com.sangola.college.model;

public class StudFee {

	Long studentaadhaar;
	Integer totalFee;
	Integer completedFee;
	
	public StudFee() {
		
	}
	
	public StudFee(Student id, int totalFee, int completedFee) {
		super();
		this.studentaadhaar = id.getStudentaadhaar();
		this.totalFee = totalFee;
		this.completedFee = completedFee;
	}
	public Long getStudentaadhaar() {
		return studentaadhaar;
	}
	public void setStudentaadhaar(Long studentaadhaar) {
		this.studentaadhaar = studentaadhaar;
	}
	public Integer getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}
	public Integer getCompletedFee() {
		return completedFee;
	}
	public void setCompletedFee(Integer completedFee) {
		this.completedFee = completedFee;
	}
	
	
	@Override
	public String toString() {
		return "studFee [studentaadhaar=" + studentaadhaar + ", totalFee=" + totalFee + ", completedFee=" + completedFee
				+ "]";
	}
	
	
	
}
