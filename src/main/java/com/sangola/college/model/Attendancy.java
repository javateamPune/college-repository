package com.sangola.college.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Attendancy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@ManyToOne
	@NotNull
	Student stud;

	@NotNull
	LocalDateTime dayTime;

	@NotNull
	Boolean isPresent;
	
	

	public Attendancy(Long id, @NotNull Student stud, @NotNull LocalDateTime dayTime, @NotNull Boolean isPresent) {
		super();
		this.id = id;
		this.stud = stud;
		this.dayTime = dayTime;
		this.isPresent = isPresent;
	}

	public Boolean getIsPresent() {
		return isPresent;
	}

	public void setIsPresent(Boolean isPresent) {
		this.isPresent = isPresent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStud() {
		return stud;
	}

	public void setStud(Student stud) {
		this.stud = stud;
	}

	public LocalDateTime getDayTime() {
		return dayTime;
	}

	public void setDayTime(LocalDateTime dayTime) {
		this.dayTime = dayTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dayTime == null) ? 0 : dayTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isPresent == null) ? 0 : isPresent.hashCode());
		result = prime * result + ((stud == null) ? 0 : stud.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attendancy other = (Attendancy) obj;
		if (dayTime == null) {
			if (other.dayTime != null)
				return false;
		} else if (!dayTime.equals(other.dayTime))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isPresent == null) {
			if (other.isPresent != null)
				return false;
		} else if (!isPresent.equals(other.isPresent))
			return false;
		if (stud == null) {
			if (other.stud != null)
				return false;
		} else if (!stud.equals(other.stud))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Attendancy [id=" + id + ", stud=" + stud + ", dayTime=" + dayTime + "]";
	}

}
