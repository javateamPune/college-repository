package com.sangola.college.model.exam;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class StudentExamMapping implements Serializable {

	private static final long serialVersionUID = -171066975162565904L;
    
	@EmbeddedId
	@NotNull
	ExamMappingId id;
	
	@NotNull
	Short marks;

	public StudentExamMapping() {
		
	}
	
	public StudentExamMapping(@NotNull ExamMappingId id, @NotNull Short marks) {
		super();
		this.id = id;
		this.marks = marks;
	}


	@JsonIgnore
	public ExamMappingId getId() {
		return id;
	}

	
	public Short getExamid() {
		return id.getExam_id();
	}

	public Long getStudentaadhaar() {
		return id.stud.getStudentaadhaar();
	}

	
	public void setId(ExamMappingId id) {
		this.id = id;
	}

	public Short getMarks() {
		return marks;
	}

	public void setMarks(Short marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "StudentExamMapping [id=" + id + ", marks=" + marks + "]";
	}

}

