package com.sangola.college.model.exam;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sangola.college.model.Student;

@Embeddable
public class ExamMappingId implements  Serializable{

	private static final long serialVersionUID = 122222L;

	
	@ManyToOne
	@NotNull
	Student stud;


	@ManyToOne
	@NotNull
	Exam exam;

	public ExamMappingId() {
		
	}
	
	public ExamMappingId(@NotNull Student stud, @NotNull Exam exam) {
		super();
		this.stud = stud;
		this.exam = exam;
	}

	@JsonIgnore
	public Student getStud() {
		return stud;
	}

	 public Short getExam_id(){
	        return exam.getId();
	 }

	 public Long getStudent_aadhaar(){
	        return stud.getStudentaadhaar();
	 }

	 
	public void setStud(Student stud) {
		this.stud = stud;
	}

	@JsonIgnore
	public Exam getExam() {
		return exam;
	}


	public void setExam(Exam exam) {
		this.exam = exam;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exam == null) ? 0 : exam.hashCode());
		result = prime * result + ((stud == null) ? 0 : stud.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamMappingId other = (ExamMappingId) obj;
		if (exam == null) {
			if (other.exam != null)
				return false;
		} else if (!exam.equals(other.exam))
			return false;
		if (stud == null) {
			if (other.stud != null)
				return false;
		} else if (!stud.equals(other.stud))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExamMappingId [stud=" + stud + ", exam=" + exam + "]";
	}

	
}
