package com.sangola.college.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class ClassAndFees {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Short id;
	
	@Column( length=30)
	@NotNull
	String className;
	
	@NotNull
	Integer totalFee;  //in rupee

	public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}

	@Override
	public String toString() {
		return "ClassAndFees [id=" + id + ", className=" + className + ", totalFee=" + totalFee + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((totalFee == null) ? 0 : totalFee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		ClassAndFees other = (ClassAndFees) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (totalFee == null) {
			if (other.totalFee != null)
				return false;
		} else if (!totalFee.equals(other.totalFee))
			return false;
		return true;
	}
		
	
	
}
