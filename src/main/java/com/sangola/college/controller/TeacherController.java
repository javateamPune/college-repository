package com.sangola.college.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sangola.college.model.Teacher;
import com.sangola.college.service.TeacherService;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
	Logger log = Logger.getLogger(TeacherController.class);

	@Autowired
	TeacherService teacherService;

	
	@GetMapping("/teachers")
	public Iterable<Teacher> getAllTeachers() {

		return teacherService.getAllTeachers();

	}

	
	@PostMapping("/teacher")
	public Teacher saveNewTeacher(Teacher teacher, String teacherDob) {

		return teacherService.createTeacher(teacher);

	}

	
	@GetMapping("/teacher")
	public Teacher getTeacher(Long aadhaar) {		
		return teacherService.getTeacher(aadhaar) ;
	
	}

	
	@DeleteMapping("/teacher")
	public void deleteTeacher(Teacher teacher) {		
		
		 teacherService.deleteTeacher(teacher);		
	}

}
