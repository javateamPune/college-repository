package com.sangola.college.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GenericController  {

	@RequestMapping("/")
	public String getHomePage() {

		return "index.html";

	}


}
