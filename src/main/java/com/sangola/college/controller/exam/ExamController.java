package com.sangola.college.controller.exam;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sangola.college.model.exam.Exam;
import com.sangola.college.service.exam.ExamService;

@RestController
@RequestMapping("/exam")
public class ExamController {

	Logger log = Logger.getLogger(ExamController.class);

	@Autowired
	ExamService examService;
	
	@GetMapping("/exams")
	public Iterable<Exam> getAllExams() {	
				
			return examService.getAllExams();
	}


	@PostMapping("/exam")
	public Exam saveNewExam(Exam exam) {
       log.info(exam);
		return examService.createExam(exam);

	}

	@GetMapping("/exam")
	public Exam getExam(Exam exam) {

			return examService.getExam(exam.getId());
		
	}

	@DeleteMapping("/exam")
	public void deleteExam(Exam exam) {
	
			examService.deleteExam(exam);

	}

	@GetMapping("/exam1.html")
	public Map getExam1(Exam exam) {

		 HashMap<String, String> map = new HashMap<>();
		    map.put("key", "value");
		    map.put("foo", "bar");
		    map.put("aa", "bb");
		    return map;
		
	}

	
}
