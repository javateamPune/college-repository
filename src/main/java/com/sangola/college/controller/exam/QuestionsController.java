package com.sangola.college.controller.exam;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sangola.college.model.exam.Questions;
import com.sangola.college.service.exam.QuestionsService;

@RestController
@RequestMapping("/exam")
public class QuestionsController {

	Logger log = Logger.getLogger(QuestionsController.class);

	@Autowired
	QuestionsService questionsService;
	
	@GetMapping("/questions")
	public Iterable<Questions> getAllQuestions() {

		return questionsService.getAllQuestions();

	}

	
	@PostMapping("/question")
	public Questions saveNewQuestion(Questions questions) {

		return questionsService.createQuestions(questions);

	}

	
	@GetMapping("/question")
	public Questions getQuestion(Questions question) {		
		
		return questionsService.getQuestions(question.getId()) ;
		
	}

	
	@DeleteMapping("/question")
	public void deleteQuestion(Questions question) {		
	
		 questionsService.deleteQuestions(question);

	}
	
}
