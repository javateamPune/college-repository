package com.sangola.college.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sangola.college.model.Event;
import com.sangola.college.service.EventService;

@RestController
@RequestMapping("/event")
public class EventController {
	Logger log = Logger.getLogger(EventController.class);

	@Autowired
	EventService eventService;
	
	@GetMapping("/events")
	public Iterable<Event> getAllEvents() {

		return eventService.getAllEvents();

	}

	@PostMapping("/event")
	public Event saveNewEvent(Event event, String endDate, String startDate) {

		return eventService.createEvent(event);

	}

	
	@GetMapping("/event")
	public Event getEvent(Short eventId) {		
		
		return eventService.getEvent(eventId) ;
	
	}

	
	@DeleteMapping("/event")
	public void deleteEvent(Event event) {		
	
		 eventService.deleteEvent(event);
		
	}
	
}
