package com.sangola.college.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sangola.college.model.Attendancy;
import com.sangola.college.model.Book;
import com.sangola.college.model.StudFee;
import com.sangola.college.model.Student;
import com.sangola.college.model.exam.Exam;
import com.sangola.college.model.exam.ExamMappingId;
import com.sangola.college.model.exam.StudentExamMapping;
import com.sangola.college.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

	Logger log = Logger.getLogger(StudentController.class);

	@Autowired
	StudentService studentService;

	@GetMapping("/students")
	public Iterable<Student> getAllStudents() {

		return studentService.getAllStudents();

	}

	
	@PostMapping("/student")
	public Student saveNewStudent(Student student, String studentDob) {

		return studentService.createStudent(student);

	}

	
	@GetMapping("/student")
	public Student getStudent(Long aadhaar) {		
		
		return studentService.getStudent(aadhaar) ;
		
	}

	
	@DeleteMapping("/student")
	public void deleteStudent(Student student) {		
		
		 studentService.deleteStudent(student);
		
	}

	@GetMapping("/exams")
	public List<StudentExamMapping>  getStudentExams(Student id) {		
	
		return  studentService.getAllExamsWithMarks(id);
		
	}
	
	
	@GetMapping("/fee")
	public StudFee getFeesReciept(Student id) {
		return studentService.getStudeFee(id);
		
	}
	
	@GetMapping("/books")
	public List<Book> getBooks(Student id) {
		
		return studentService.getStudBooks(id);
		
	}
	
	
	@PostMapping("/attendancy")
	public Attendancy saveAttendancy(Attendancy attendancy) {
		
		return studentService.saveAttendance(attendancy);
		
	}
	
	@GetMapping("/attendancy")
	public List<Attendancy> getAttendancy(Student id) {		
		return studentService.getAttendance(id);		
	}
	
	@GetMapping("/marks")
	public StudentExamMapping getMarks(Student sid,Exam id) {				
		ExamMappingId ex = new ExamMappingId(sid,id);
		
		log.info(ex);
		return studentService.getExamMarks(ex);
	}
	
	@PostMapping("/marks")
	public StudentExamMapping saveMarks(Student sid,Exam id,Short marks) {				
		ExamMappingId ex = new ExamMappingId(sid,id);
		StudentExamMapping exm = new StudentExamMapping(ex,marks);
		log.info(ex);
		return studentService.saveExamMarks(exm);
	}
	
	
}
