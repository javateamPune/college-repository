package com.sangola.college;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollegeApplication {

	static Logger log = Logger.getLogger(CollegeApplication.class);
	
	public static void main(String[] args) {
	 
		SpringApplication.run(CollegeApplication.class, args);
			
		log.info("-----------------------Application End --------------------------");	
		
	}

}
