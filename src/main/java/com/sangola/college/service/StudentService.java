package com.sangola.college.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangola.college.model.Attendancy;
import com.sangola.college.model.Book;
import com.sangola.college.model.StudFee;
import com.sangola.college.model.Student;
import com.sangola.college.model.exam.ExamMappingId;
import com.sangola.college.model.exam.StudentExamMapping;
import com.sangola.college.repository.AttendancyRepository;
import com.sangola.college.repository.StudentRepository;
import com.sangola.college.repository.exam.StudentExamMappingRepository;

@Service
public class StudentService {

	Logger log = Logger.getLogger(StudentService.class);
	
	@Autowired
	StudentRepository studentRepo;

	@Autowired
	StudentExamMappingRepository studExamRepo;
	
	@Autowired
	AttendancyRepository AttendancyRepo;
	
	
	public Student getStudent(Long aadhaar) {

		return studentRepo.findById(aadhaar).get();
	}

	public Iterable<Student> getAllStudents() {

		return studentRepo.findAll();
	}

	public Student createStudent(Student student) {

		return studentRepo.save(student);

	}

	public void deleteStudent(Student student) {

		studentRepo.delete(student);

	}

	public void updateStudent(Student student) {

		studentRepo.save(student);
	}

	public StudentExamMapping getExamMarks(ExamMappingId id) {
		
		return studExamRepo.findById(id).get();
		
	}
	
	public StudentExamMapping saveExamMarks(StudentExamMapping marks) {		
		return studExamRepo.save(marks);
	}
	
   public List<StudentExamMapping>  getAllExamsWithMarks(Student  stud) {
	   
	 return  studExamRepo.findByIdStud(stud);
	
   }
   

   /**
    * Yet to implement
    * 
    * 
    * @return fees     
    */    
	
   public StudFee getStudeFee(Student id) {
	   
	   
	   return new StudFee(id,100,100);

   }
   
 /**
  * Yet to implement
  *     
  * @param id
  * @return  book list as List<Book>
  */
	
   public List<Book> getStudBooks(Student id) {

	   List<Book> bookList= new ArrayList<Book>();
	   bookList.add(new Book("123", "Core Java", "R Nageshawra Rao", LocalDate.now()));
	   bookList.add(new Book("124", "Core Java", "R Nageshawra Rao", LocalDate.now()));
	   
	   return  bookList;

   }
      
   
   public Attendancy saveAttendance(Attendancy attend) {
	   
	      
	   return AttendancyRepo.save(attend);
	    
   }
   

   public List<Attendancy> getAttendance(Student id) {
	   
	   
	   return AttendancyRepo.findByStud(id);
	    
   }
   
   
}
