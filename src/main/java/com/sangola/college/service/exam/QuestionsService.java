package com.sangola.college.service.exam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangola.college.model.exam.Questions;
import com.sangola.college.repository.exam.QuestionsRepository;

@Service
public class QuestionsService {


	@Autowired
	QuestionsRepository questionsRepo;

	public Questions getQuestions(Integer id) {

		return questionsRepo.findById(id).get();
	}

	public Iterable<Questions> getAllQuestions() {

		return questionsRepo.findAll();
	}

	public Questions createQuestions(Questions questions) {

		return questionsRepo.save(questions);

	}

	public void deleteQuestions(Questions questions) {

		questionsRepo.delete(questions);

	}

	public void updateQuestions(Questions questions) {

		questionsRepo.save(questions);
	}

	
}
