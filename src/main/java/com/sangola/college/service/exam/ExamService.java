package com.sangola.college.service.exam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangola.college.model.exam.Exam;
import com.sangola.college.model.exam.StudentExamMapping;
import com.sangola.college.repository.exam.ExamRepository;
import com.sangola.college.repository.exam.StudentExamMappingRepository;

@Service
public class ExamService {

	

	@Autowired
	ExamRepository examRepo;

	@Autowired
	StudentExamMappingRepository studExam;
	
	public Exam getExam(Short id) {

		return examRepo.findById(id).get();
	}

	public Iterable<Exam> getAllExams() {

		return examRepo.findAll();
	}

	public Exam createExam(Exam exam) {

		return examRepo.save(exam);

	}

	public void deleteExam(Exam exam) {

		examRepo.delete(exam);

	}

	public void updateExam(Exam exam) {

		examRepo.save(exam);
	}

	public Iterable<StudentExamMapping> getAllExamMarks(  ) {

		return studExam.findAll();
	}
	
}
