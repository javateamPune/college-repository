package com.sangola.college.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangola.college.model.Teacher;
import com.sangola.college.repository.TeacherRepository;

@Service
public class TeacherService {


	@Autowired
	TeacherRepository teacherRepo;

	public Teacher getTeacher(Long aadhaar) {

		return teacherRepo.findById(aadhaar).get();
	}

	public Iterable<Teacher> getAllTeachers() {

		return teacherRepo.findAll();
	}

	public Teacher createTeacher(Teacher teacher) {

		return teacherRepo.save(teacher);

	}

	public void deleteTeacher(Teacher teacher) {

		teacherRepo.delete(teacher);

	}

	public void updateTeacher(Teacher teacher) {

		teacherRepo.save(teacher);
	}

	
}
