package com.sangola.college.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangola.college.model.Event;
import com.sangola.college.repository.EventRepository;

@Service
public class EventService {

	@Autowired
	EventRepository eventRepo;

	public Event getEvent(Short eventId) {

		return eventRepo.findById(eventId).get();
	}

	public Iterable<Event> getAllEvents() {

		return eventRepo.findAll();
	}

	public Event createEvent(Event event) {

		return eventRepo.save(event);

	}

	public void deleteEvent(Event event) {

		eventRepo.delete(event);

	}

	public void updateEvent(Event event) {

		eventRepo.save(event);
	}

}
