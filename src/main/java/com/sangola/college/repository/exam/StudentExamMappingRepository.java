package com.sangola.college.repository.exam;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.Student;
import com.sangola.college.model.exam.ExamMappingId;
import com.sangola.college.model.exam.StudentExamMapping;


public interface StudentExamMappingRepository  extends CrudRepository<StudentExamMapping,ExamMappingId> {

	
	List<StudentExamMapping> findByIdStud(Student id);

	//@Query("SELECT e from   StudentExamMapping e where e.id = ?1   ")
	//List<StudentExamMapping> findMarks(Student stud);
	
	
	
	
}
