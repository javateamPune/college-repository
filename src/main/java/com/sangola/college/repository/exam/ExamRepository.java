package com.sangola.college.repository.exam;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.exam.Exam;

public interface ExamRepository extends CrudRepository<Exam, Short > {

}
