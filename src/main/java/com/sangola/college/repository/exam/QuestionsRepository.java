package com.sangola.college.repository.exam;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.exam.Questions;

public interface QuestionsRepository extends CrudRepository<Questions,Integer> {

}
