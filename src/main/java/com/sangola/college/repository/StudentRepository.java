package com.sangola.college.repository;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
