package com.sangola.college.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.Attendancy;
import com.sangola.college.model.Student;

public interface AttendancyRepository  extends CrudRepository<Attendancy,Long> {

	List<Attendancy>  findByStud(Student stud);
	
}
