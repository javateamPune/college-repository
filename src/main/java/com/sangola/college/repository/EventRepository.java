package com.sangola.college.repository;

import org.springframework.data.repository.CrudRepository;

import com.sangola.college.model.Event;

public interface EventRepository extends CrudRepository<Event, Short> {

	
	
	
}
