package com.sangola.college.repository;

import org.springframework.data.repository.CrudRepository;
import com.sangola.college.model.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, Long> {

}
